# Agenda

| When                           | Where                                         | Attendees                       |
| ------------------------------ | --------------------------------------------- | ------------------------------- |
| Monday 25 Nov 2019 at 09:30 am | Gina's house<br /> 29 Taylor Avenue, Lockleys | Gina Brooks<br />Leticia Mooney |

## Business

### 1. Webinars
   1. Dates each month that Gina is available to run a breakfast webinar
   2. Topics (what are they, what sequence)
   3. Giveaway document for each one
### 2. Blogs to Publications
   1. Status update: What's with the website?
   2. Blogs to lead magnets: Status update
   3. Publishing project plan: Status update
### 3. Email Strategy
   1. SA Leaders, Daily Emails
   2. Squeeze Page update
### 4. Agreed response times (Gina/Leticia)
### 5. Gina's Must Dos: On the last day of each month

